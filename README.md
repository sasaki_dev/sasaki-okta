![build status](https://www.codeship.io/projects/7a313ea0-b1f0-0131-7802-4a1c64f0cfbd/status)

# Sasaki Okta API methods

An abstraction layer for commonly used [Okta API][4] methods at Sasaki.

Please use the [issue tracker] to report any bugs.


## Features:

* implements a subset of [Okta's Users and Groups API][4]
* provides a middleware to protect routes with [Okta's SAML authentication][8]; see [Express.js] example below
* follows loosely the [JSON API] specification for response objects
* methods return [Promises][3]


## Installation

    npm install sasaki-okta


## Documentation

The module expects following environment variables:

    OKTA_APIKEY                      ... [required] Okta API key
    OKTA_DOMAIN                      ... [required] <DOMAIN>.okta.com

### Okta API methods

    getUser(<id>)                    ...  Get single user for given Okta ID
    getCurrentUser()                 ...  Get current user linked to API token or session cookie
    getUsers()                       ...  Get user list
    getUsersByGroup(<id>)            ...  Get user list for given Okta Group ID
    isUserInGroup(<userId, groupId>) ...  Check if single user is in the group
                                          for given Okta User ID and Okta
                                          Group ID

#### Response objects loosely follow the [JSON API] specs.

    { user: {...} }                  ...   for single user requests
    { users: [{...}, {...}] }        ...   for a user list request


### Okta (SAML) Authentication

    auth(<config>[, <passport>])     ...  Instantiate passport authentication
                                          with SAML strategy. Accepts an
                                          optional `passport` instance to
                                          workaround the (default) passport
                                          singleton, e.g. in case of multiple
                                          apps with different authentication
                                          requirements such as vhosts.
                                          Returns a `passport` instance.

    auth.protected                   ...  Middleware to validate authentication
                                          and redirect to `/login` route in case
                                          of non-authenticated session.


## Examples

Basic example:

    var okta = require('sasaki-okta');
    okta.getUsers().then(function(data) {
      // do something with data
    }, function(error) {
      // error handling
    });

Basic authentication integration with [Express][7]:

    // Authentication setup
    var okta = require('sasaki-okta');
    var oktaConfig = {
      issuer: '<ISSUER>',
      entryPoint: '<ENTRY_POINT>',
      cert: '<CERT>'
    }
    var auth = okta.auth(oktaConfig);
    // Pass passport-instance as argument to avoid passport singleton in multiple apps (e.g. vhost scenario)
    // var Passport = require('passport').Passport;
    // var passport = new Passport();
    // var auth = okta.auth(oktaConfig, passport);

    // Express app
    var express = require('express');
    var session = require('express-session');
    var app = express();
    app.use(session({
      secret: '<SECRET>',
      resave: false,
      saveUninitialized: true,
      cookie: {}
    }))
    app.use(auth.initialize());
    app.use(auth.session());

    // Receives Okta SAML assertion
    app.post('/login/callback', auth.authenticate('saml', { failureRedirect: '/', failureFlash: true }), function (req, res) {
        res.redirect(302, '<REDIRECT_URL>');
      });
    // Login URL
    app.get('/login', auth.authenticate('saml', { failureRedirect: '/', failureFlash: true }), function (req, res) {
        res.redirect(302, '<REDIRECT_URL>');
    });

    // Protected route
    app.get('/privatedata', auth.protected, function(req, res) {
      res.send({message: 'This is private.'});
    });



## Authors

* [Christian Spanring][1]
* [Yueying Cui][6]
* [Raj Adi Raman][7]


[1]: mailto:cspanring@sasaki.com
[JSON API]: http://jsonapi.org/
[3]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
[4]: http://developer.okta.com/docs/api/rest/users.html
[6]: mailto:ycui@sasaki.com
[7]: mailto:tadiraman@sasaki.com
[Express.js]: http://expressjs.com/
[issue tracker]: https://bitbucket.org/sasaki_dev/sasaki-okta/issues
