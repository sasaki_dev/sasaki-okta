var users = [];


function findByEmail(email, cb) {
  for (var i = 0, len = users.length; i < len; i++) {
    var user = users[i];
    if (user.email === email) {
      return cb(null, user);
    }
  }
  return cb(null, null);
}


/**
 * Instantiate Passport authentication, either using a given passport instance
 * or default to global passport singleton
 * @param  {Object} config       Okta configuration object: `issuer`,
 *                               `entryPoint` and `cert`
 * @param  {Object} app_passport Passport instance
 * @return {Object}              Passport instance with SAML Strategy
 */
function auth(config, app_passport) {
  var passport = app_passport || require('passport'),
      SamlStrategy = require('passport-saml').Strategy;


  passport.serializeUser(function(user, done) {
    done(null, user.email);
  });

  passport.deserializeUser(function(id, done) {
    findByEmail(id, function (err, user) {
      done(err, user);
    });
  });

  passport.use(new SamlStrategy(
    {
      issuer: config.issuer,
    	path: '/login/callback',
      entryPoint: config.entryPoint,
      cert: config.cert
    },
    function(profile, done) {
      if (!profile.email) {
        return done(new Error("No email found"), null);
      }
      process.nextTick(function () {
        findByEmail(profile.email, function(err, user) {
          if (err) {
            return done(err);
          }
          if (!user) {
            users.push(profile);
            return done(null, profile);
          }
          return done(null, user);
        });
      });
    }
  ));

  passport.use('search_saml', new SamlStrategy(
    {
      issuer: config.issuer,
      path: '/login/callback',
      entryPoint: config.entryPoint,
      cert: config.cert
    },
    function(profile, done) {
      if (!profile.email) {
        return done(new Error("No email found"), null);
      }
      process.nextTick(function () {
        findByEmail(profile.email, function(err, user) {
          if (err) {
            return done(err);
          }
          if (!user) {
            users.push(profile);
            return done(null, profile);
          }
          return done(null, user);
        });
      });
    }
  ));

  passport.protected = function protected(req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    }
    res.redirect('/login');
  };

  return passport;
}


module.exports = auth;
