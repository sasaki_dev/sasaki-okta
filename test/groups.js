/* global describe, it */

var okta = require('../'),
    chai = require('chai'),
    should = chai.should(),
    expect = chai.expect,
    chaiAsPromised = require("chai-as-promised"),

    groupId = process.env.OKTA_TEST_GROUP_ID,
    userInGroupId = process.env.OKTA_TEST_GROUP_USER_IN_GROUP,
    userNotInGroupId = process.env.OKTA_TEST_USER_NOT_IN_GROUP;


// Test promises
chai.use(chaiAsPromised);


describe('Okta Group API', function () {
  it('should respond with a user list', function () {
    return expect(okta.getUsersByGroup(groupId)).to.eventually.have.property('users').with.property('length').to.be.above(0);
  });

  it('should have user in group', function() {
    return okta.isUserInGroup(userInGroupId, groupId).should.become(true);
  });

  it('should not have user in group', function() {
    return okta.isUserInGroup(userNotInGroupId, groupId).should.become(false);
  });
});
