/* global describe, it */

var okta = require('../'),
    chai = require('chai'),
    should = chai.should(),
    expect = chai.expect,
    chaiAsPromised = require("chai-as-promised"),

    userId = process.env.OKTA_TEST_USER_ID,
    userEmail = process.env.OKTA_TEST_USER_EMAIL;


// Test promises
chai.use(chaiAsPromised);


describe('Okta User API', function () {
  it('should respond with a single user', function () {
    return expect(okta.getUser(userId)).to.eventually.have.deep.property('user.email', userEmail);
  });

  it('should return a list of all users', function () {
    this.timeout(20000);
    return expect(okta.getUsers()).to.eventually.have.property('users').with.property('length').to.be.above(0);
  });

  it('should respond with the correct current user', function () {
    return expect(okta.getCurrentUser()).to.eventually.have.deep.property('user.email', userEmail);
  });

  it('should return a list of filtered users', function () {
    this.timeout(20000);
    return expect(okta.getUsers({filter: 'status eq "ACTIVE"'})).to.eventually.have.property('users').with.property('length').to.be.above(0);
  });
});
