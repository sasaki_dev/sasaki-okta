/* global describe, it */

var okta = require('../'),
    chai = require('chai'),
    should = chai.should(),

    Passport = require('passport').Passport,
    passport = new Passport(),
    config = {
      issuer: 'issuer',
      entryPoint: 'entryPoint',
      cert: 'cert'
    };


describe('Default Authentication', function () {
  var auth = okta.auth(config);

  it('is a passport instance', function(done) {
    auth.should.be.an.instanceof(Passport);
    done();
  });
  it('exposes required initialization methods', function (done) {
    auth.initialize.should.to.be.a('function');
    auth.session.should.to.be.a('function');
    auth.protected.should.to.be.a('function');
    done();
  });
  it('has a SAML Strategy', function (done) {
    auth.should.have.deep.property('_strategies.saml');
    done();
  });
});


describe('Authentication with custom passport instance', function () {
  var auth = okta.auth(config, passport);

  it('is a passport instance', function(done) {
    auth.should.be.an.instanceof(Passport);
    done();
  });
  it('exposes required initialization methods', function (done) {
    auth.initialize.should.to.be.a('function');
    auth.session.should.to.be.a('function');
    auth.protected.should.to.be.a('function');
    done();
  });
  it('has a SAML Strategy', function (done) {
    auth.should.have.deep.property('_strategies.saml');
    done();
  });
});
