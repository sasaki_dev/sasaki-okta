
/**
 * Created by tadiraman on 5/19/2017.
 */
/**
 * Created by ycui on 1/13/2016
 * Bitbucket: https://bitbucket.org/sasaki_dev/pando/tonic/smartsheet_updaterequests.js
 *
 * Output:
 *  TEST: send emails to the testEmail (subject: the original email address)
 *  PRODUCT: send email to the original email address
 */


// variables
var token = process.env.TOKEN_TEST; // TEST: TOKEN_TEST, TOKEN_PRODUCT
var sheetId = '8705729361143684'; // TEST: 8705729361143684, PRODUCTION: 4618880611051396
var emailColumnId = '4241946865624964'; // TEST: 4241946865624964, PRODUCTION: 8726684405917572
var accessColumnIds = ['8041859051218820', '5227109284112260']; // ['Draft Plan Complete', 'Draft Plan Due Date']. TEST: ['8041859051218820', '5227109284112260'], PRODUCTION: ['286438081226628', '7552474706929540']
var subject = 'Testing Script';
var message = 'Test Message Body';

var env = 'PRODUCT'; // TEST, PRODUCT
var testEmail = 'ycui@sasaki.com';

// API functions
var request = require('request');

/!**
 * Get rowIds and emails from a Smartsheet
 * @param {String} token    Smartsheet API token
 * @param {String} sheetId  Smartsheet ID
 * @param {String} columnId Smartsheet receiver's email column ID
 * @return {object}         {rowId: email}
 *!/
function getColumn(token, sheetId, columnId) {
  var options = {
    url: 'https://api.smartsheet.com/2.0/sheets/' + sheetId +'?exclude&columnIds=' + columnId,
    headers: {
      'Authorization': 'Bearer ' + token
    }
  };
  var ans = {};

  return new Promise(function(resolve, reject) {
    request(options, function (error, response, body) {
      if (error || response.statusCode !== 200) {
        console.log("Error: " + response.statusCode);
      } else {
        var data = JSON.parse(body);
        if (data.rows) {
          data.rows.forEach(function (row, i) {
            ans[row.id] = row.cells[0].value;
          });
        }
      }
      resolve(ans);
    });
  });

}

/!**
 * Send update requests to the email list
 * exposing only the access columns
 * @param {String} token            Smartsheet API token
 * @param {String} sheetId          Smartsheet ID
 * @param {Array} accessColumnIds   accessable Smartsheet column IDs
 * @param {object} emails           {rowId, email}
 * @param {String} subject          email subject
 * @param {String} message          email message
 * @param {String} env              node environment: TEST, PRODUCT
 * @param {String} testEmail        testing receiver email
 * @return {Integer}                total number of emails have been sent
 *!/
function sendEmails(token, sheetId, accessColumnIds,
                    emails, subject, message,
                    env, testEmail) {
  var total = Object.keys(emails).length;
  if (total === 0) {
    return;
  }
  return new Promise(function(resolve, reject) {
    var count = 0;
    Object.keys(emails).forEach(function (rowId) {
      var options = {
        method: 'POST',
        url: 'https://api.smartsheet.com/2.0/sheets/' + sheetId +'/updaterequests',
        headers: {
          'Authorization': 'Bearer ' + token
        },
        json: {
          "sendTo": [
            {"email": emails[rowId]}
          ],
          "subject": subject,
          "message": message,
          "rowIds": [rowId],
          "columnIds": accessColumnIds,
          "includeAttachments": "true",
          "includeDiscussions": "false"
        }
      };

      if (env === 'TEST') {
        options.json.sendTo = [{"email": testEmail}];
        options.json.subject = emails[rowId];
      }
      request(options, function (error, response, body) {
        if (error || response.statusCode !== 200) {
          console.log("Error: " + response.statusCode + ", " + emails[rowId]);
        }
        count++;
        if (count === total) {
          resolve(total);
        }
      });
    });
  });
}

// execution
console.log("running environment: " + env);
var emails = await getColumn(token, sheetId, emailColumnId);
console.log("emails: " + Object.keys(emails).length, emails);

var total = await sendEmails(token, sheetId, accessColumnIds, emails, subject, message, env, testEmail);
console.log('done! total: ' + total);

