var _ = require('lodash'),
  request = require('request'),
  RSVP = require('rsvp'),

  config = require('./config.js'),
  auth = require('./lib/auth.js'),

  okta;


/**
 * Throw exception if API key is not present.
 */
if (!config.apiKey) {
  throw 'Okta API key is not set.';
}


/**
 * Serializes Okta's user object into a simpler version,
 * exposing only user-id and user-profile.
 * @private
 * @param  {Object} obj  Okta user object
 * @return {Object}      Simple user object
 */
function _serializeSimpleUser(obj) {
  return _.merge({id: obj.id}, obj.profile);
}

var getURL = function (rawLink) {
  var start_pos = rawLink.indexOf('<') + 1;
  var end_pos = rawLink.indexOf('>', start_pos);
  var text_to_get = rawLink.substring(start_pos, end_pos);
  return text_to_get;
};

/* Get the url from header link */
var getNextLink = function (link) {
  if (link.indexOf('rel="next"') > -1) {
    var links = link.split(',');
    if (links[1] && links[1].indexOf('rel="next"') > -1) {
      return getURL(links[1]);
    } else {
      return getURL(links[0]);
    }
  } else {
    return null;
  }
};

var usersArr = '';
var requestByPagination = function (url, qs, requestHeaders, resolve, reject) {
  request({
    method: 'GET',
    url: url,
    headers: requestHeaders,
    qs: qs
  }, function (error, response, body) {
    var data = JSON.parse(body);
    if (!error && response.statusCode === 200) {
      if (_.isArray(usersArr)) {
        usersArr = usersArr.concat(data);
      } else {
        usersArr = data;
      }
      //fetch the url for next set of users
      if (response.headers && response.headers.link) {
        var nextLink = getNextLink(response.headers.link);
        if (nextLink != null) {
          requestByPagination(nextLink, qs, requestHeaders, resolve, reject);
        }else {
          if (_.isPlainObject(usersArr)) {
            resolve([_serializeSimpleUser(usersArr)]);
          } else { // it's an Array
            resolve(_.map(usersArr, function (user) {
              return _serializeSimpleUser(user);
            }));
          }
        }
      }
      else {
        if (_.isPlainObject(usersArr)) {
          resolve([_serializeSimpleUser(usersArr)]);
        } else { // it's an Array
          resolve(_.map(usersArr, function (user) {
            return _serializeSimpleUser(user);
          }));
        }
      }
    } else if (data.errorCode) {
      reject('Error ' + data.errorCode + ', ' + data.errorSummary);
    } else {
      reject('Something went wrong.');
    }
  });
};


/**
 * Query Okta API for a user list.
 * @private
 * @param  {String} url     URL to query Okta API
 * @param  {Object} params  url parameters for Okta API
 * @return {Array}          Promise resolving into collection of simple user
 *                          objects
 */
function _getOktaUsers(url, params) {
  var qs = params || {},
    requestHeaders = {
      'Authorization': 'SSWS ' + config.apiKey,
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    };

  return new RSVP.Promise(function (resolve, reject) {
    requestByPagination(url, qs, requestHeaders, resolve, reject);
  }).then(function (users) {
    return users;
  });
}


okta = {

  /**
   * Returns list of all users.
   * @return {Array}     Promise resolving into a simple user collection
   */
  getUsers: function (params) {
    return _getOktaUsers(config.apiUrl + '/users?limit=1000', params).then(function (users) {
      return {users: users};
    });
  },


  /**
   * Return user for given ID from Okta
   * @param  {String} id Okta User ID
   * @return {Object}    Promise resolving into simple user object
   */
  getUser: function (id) {
    return _getOktaUsers(config.apiUrl + '/users/' + id).then(function (users) {
      console.log('users: ' + JSON.stringify(users));
      return {user: users[0]};
    });
  },


  /**
   * Returns current user linked to API token or session cookie
   * @return {Object}    Promise resolving into simple user object
   */
  getCurrentUser: function () {
    return _getOktaUsers(config.apiUrl + '/users/me').then(function (users) {
      return {user: users[0]};
    });
  },


  /**
   * Return if the user belongs to the group
   * @param {String} userId  Okta User ID
   * @param {String} groupId Okta Group ID
   * @return {Bool}          true/false
   */
  isUserInGroup: function (userId, groupId) {
    return this.getUsersByGroup(groupId).then(function (data) {
      return _.some(data.users, {'id': userId});
    });
  },

  /**
   * Return users for given Group ID from Okta
   * @param  {String} id Okta Group ID
   * @return {Object}    Promise resolving into simple user object
   */
  getUsersByGroup: function (id) {
    return _getOktaUsers(config.apiUrl + '/groups/' + id + '/users').then(function (users) {
      return {users: users};
    });
  },


  /**
   * Alias for Passport authentication with SAML strategy
   */
  auth: auth
};
module.exports = okta;
