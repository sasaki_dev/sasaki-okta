module.exports = {

  /**
  * Okta API Key, requires OKTA_APIKEY environment variable
  * @type {String}
  */
  apiKey: process.env.OKTA_APIKEY,


  /**
  * Okta API URL
  * @type {String}
  */
  apiUrl: 'https://' + process.env.OKTA_DOMAIN + '.okta.com/api/v1'
};
